<div class="section-date-author"><?php print ucfirst ($type); ?> | 
<?php print date('F j, Y', $node->created); ?>
 | By  
<?php  
 $author = user_load(array('uid' => $node->uid));
 $author_name = ($author->profile_full_name) ? $author->profile_full_name : $author->name;
 print l($author_name, 'user/'.$node->uid);
?>	
</div>
<div class="body-content">
<?php  
  if ($field_main_image[0][filepath]) {
  	print '<div class="main-image">'.theme('imagecache', '400xY',$field_main_image[0][filepath], $field_main_image[0][data][description], $field_main_image[0][data][description]).'</div>';
  }
  print $body;     
  if ($field_show_author_info[0][value] &&  $author->profile_full_name) {
?>
    <div class="user-profile">
    	<h3>About the Author</h3>
      <?php if ($author->picture) print theme('image', $author->picture); ?>
      <div class="user-name"><?php print $author->profile_full_name; ?></div>
      <div class="user-job-title"><?php print $author->profile_job_title; ?></div>
      <div class="user-bio"><?php print $author->profile_bio; ?></div>	  
    </div>
	<div class="clear"><br/></div>
<?php
  }
?>
</div>
<?php if ($calais_geo_map): ?>
<div class="google-map">
	 <?php print $calais_geo_map; ?>
</div>
<?php endif; ?>
<?php print related_terms_links($taxonomy); ?>
<?php print theme('links', $node->links); ?>