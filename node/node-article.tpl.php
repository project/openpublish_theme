<?php if ($field_deck): ?>
	<div class="deck"><?php print $field_deck[0][value]; ?></div>
<?php endif; ?>
<div class="section-date-author">Article | 
<?php print date('F j, Y', $node->created); ?>
 | By  
<?php  
  $author = user_load(array('uid' => $node->uid));
  print l($author->profile_full_name, 'user/'.$node->uid);  
?>	
</div>
<div class="body-content">
<?php
  $main_image_credit = $field_main_image_credit[0]['view'];
  $main_image_title  = $field_main_image[0][data][title];
  $main_image_desc  = $field_main_image[0][data][description];
  $main_image = theme('imagecache','400xY',$field_main_image[0][filepath], $main_image_title, $main_image_title);
  if ($field_main_image[0][filepath]) : ?>
        <div class="main-image">
      <?php print $main_image ?>
      <div class="main-image-desc image-desc">
        <?php print $main_image_desc ?> <span class="main-image-credit image-credit"><?php print $main_image_credit ?></span>
      </div>
    </div>
  <?php endif; ?>
  <?php
  print $body;     
  if ($field_show_author_info[0][value] && $author->profile_full_name) {
  ?>
    <div class="user-profile">
    	<h3>About the Author</h3>
      <?php if ($author->picture) print theme('image', $author->picture); ?>
      <div class="user-name"><?php print $author->profile_full_name; ?></div>
			<div class="user-job-title"><?php print $author->profile_job_title; ?></div>
      <div class="user-bio"><?php print $author->profile_bio; ?></div>	  
    </div>
	<div class="clear"><br/></div>
<?php
  }
?>
</div>
<?php if ($calais_geo_map): ?>
<div class="google-map">
	 <?php print $calais_geo_map; ?>
</div>
<?php endif; ?>
<?php print related_terms_links($taxonomy); ?>
<?php print theme('links', $node->links); ?>
