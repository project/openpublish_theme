<div class="news-package">
  <div id="news-package-left">
    <?php print $field_left_intro[0][view]; ?>
	<?php
  	if (is_array($field_left_related) && !empty($field_left_related)) {  		
  	  foreach($field_left_related as $id => $left_related) {
  		$related_node = node_load($left_related[nid]);  		
  		print '<div class="news-package-left-related">';
  		if ($related_node->field_main_image[0][filepath]) {
	      print theme('imagecache', 'thumbnail', $related_node->field_main_image[0][filepath], $related_node->field_main_image[0][data][description], $related_node->field_main_image[0][data][description]);
	    }
  		print '<div class="package-related-article-title">'.l($related_node->title, 'node/'.$related_node->nid).'</div>';
	    print '<div class="package-related-article-teaser">'.$related_node->teaser.'</div>';
	    print '</div>';
  	  }
  	}
  	?>
  </div>
  <div id="news-package-center">
    <?php print $field_center_intro[0][view]; ?>
	<?php 	  	  
	  if ($field_embedded_video[0][view]) print $field_embedded_video[0][view];
	  else if ($field_embedded_audio[0][view]) print $field_embedded_audio[0][view];
	  else if ($field_center_main_image[0][filepath]) {
	    print theme('imagecache', 'spotlight_homepage', $field_center_main_image[0][filepath], $field_center_main_image[0][data][description], $field_center_main_image[0][data][description]);	
	  }
	?>
	<br/>
	<?php
  	if (is_array($field_center_related) && !empty($field_center_related)) {  		
  	  foreach($field_center_related as $id => $center_related) {
  		$related_node = node_load($center_related[nid]);  		
  		print '<div class="news-package-center-related">';
  		if ($related_node->field_main_image[0][filepath]) {
	      print theme('imagecache', 'thumbnail', $related_node->field_main_image[0][filepath], $related_node->field_main_image[0][data][description], $related_node->field_main_image[0][data][description]);
	    }
  		print '<div class="package-related-article-title">'.l($related_node->title, 'node/'.$related_node->nid).'</div>';
	    print '<div class="package-related-article-teaser">'.$related_node->teaser.'</div>';
	    print '</div>';
  	  }
  	}
  	?>
  </div>
  <div id="news-package-right">
    <?php print $field_right_intro[0][view]; ?>
	<?php
  	if (is_array($field_right_related) && !empty($field_right_related)) {  		
  	  foreach($field_right_related as $id => $right_related) {
  		$related_node = node_load($right_related[nid]);  		
  		print '<div class="news-package-right-related">';
  		if ($related_node->field_main_image[0][filepath]) {
	      print theme('imagecache', 'thumbnail', $related_node->field_main_image[0][filepath], $related_node->field_main_image[0][data][description], $related_node->field_main_image[0][data][description]);
	    }
  		print '<div class="package-related-article-title">'.l($related_node->title, 'node/'.$related_node->nid).'</div>';
	    print '<div class="package-related-article-teaser">'.$related_node->teaser.'</div>';
	    print '</div>';
  	  }
  	}
  	?>
  </div>  
</div>