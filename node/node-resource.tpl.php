<div class="section-date-author"><?php print ucfirst ($type); ?></div>
<div class="body-content">
  <?php print $body; ?>
</div>
<div class="resource-links">
<?php 
if (is_array($field_links) && !empty($field_links)) {
	$output = '<ul>';
  foreach($field_links as $link) {
  	$output .= '<li>'.$link[view].'</li>';
  }	
  $output .= '</ul>';
  print $output;
}
?>
</div>
<?php print related_terms_links($taxonomy); ?>
