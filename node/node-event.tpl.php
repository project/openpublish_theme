<div class="section-date-author"><?php print ucfirst ($type); ?></div>
<div class="body-content">
<?php
  $main_image_credit = $field_main_image_credit[0]['view'];
  $main_image_title  = $field_main_image[0][data][title];
  $main_image_desc  = $field_main_image[0][data][description];
  $main_image = theme('imagecache','400xY',$field_main_image[0][filepath], $main_image_title, $main_image_title);
  if ($field_main_image[0][filepath]) : ?>
        <div class="main-image">
      <?php print $main_image ?>
      <div class="main-image-desc image-desc">
        <?php print $main_image_desc ?> <span class="main-image-credit image-credit"><?php print $main_image_credit ?></span>
      </div>
    </div>
<?php endif; ?>
<div class="event-date">Event Date: <?php print $field_event_date[0]['view']; ?></div>
<?php print $node->content['body']['#value']; ?>
</div>
<?php print related_terms_links($taxonomy); ?>