<?php
/**
 * @file page.tpl.php
 * Main generic page template.
 * @ingroup page
 */
?>
<?php require_once(dirname(__FILE__) . '/page.header.inc'); ?>

<?php if ($left): ?>
  <div id="sidebar-left" class="sidebar">
  	<?php print $left ?>
  </div> <!-- /#sidebar-left -->
<?php endif; ?>

<div id="center">
  <?php print $breadcrumb; ?>
  <br/>
  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
  <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
  <?php if ($title && $node->type != 'topichub'): ?>
  	<div id="print-this" class="float-right">&nbsp;&nbsp;&nbsp;<a href="#" onClick="print(); return false;">Print</a></div>
  	<div id="add-this" class="float-right">
      <?php print phptemplate_addthis_widget(); ?>
  	</div>
    <?php	print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; ?>
  <?php endif; ?>
  <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
  <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
  <?php if ($show_messages && $messages): print $messages; endif; ?>
  <?php print $help; ?>
  
  <div id="op-over-content">
    <?php print $over_content; ?>    
  </div>
  
  <?php print $content ?>
  
  <div id="op-under-content">
    <?php print $under_content ?>  
  </div>
  
</div> <!-- /#center -->

<?php if ($right): ?>
  <div id="sidebar-right" class="sidebar">
  	<?php print $right ?>
  </div> <!-- /#sidebar-right -->
<?php endif; ?>
        
<?php require_once(dirname(__FILE__) . '/page.footer.inc');