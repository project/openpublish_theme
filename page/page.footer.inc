        <div class="clear"></div>	
         	
        <div id="footer" class="clear">
          <div id="footer-menu-primary"><?php print menu_tree('menu-footer-primary'); ?></div>
          <div id="footer-feed-icon"><a href="<?php print url('rss/articles/all'); ?>"><img src="/misc/feed.png" width="16" height="16" alt="Get the main site RSS feed"/></a></div>
          <div id="footer-menu-secondary"><?php print menu_tree('menu-footer-secondary'); ?></div>
          <div class="clear"></div>
          <?php print $footer_message . $footer ?>          
        </div> <!-- /#footer -->
      </div> <!-- /#container -->
      <span class="clear"></span>
    </div> <!-- /#wrapper -->
  </div> <!-- /#outer-wrapper -->
  <!-- /layout -->
  <?php print $closure ?>
</body>
</html>
