<?php
/**
 * @file page.header.inc
 * Header template.
 * @ingroup page
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html 
     xmlns="http://www.w3.org/1999/xhtml" 
     xmlns:dc="http://purl.org/dc/elements/1.1/" 
     xmlns:ctag="http://commontag.org/ns#" 
     lang="<?php print $language->language; ?>" 
     dir="<?php print $language->dir; ?>"
     version="XHTML+RDFa 1.0" >
<head>
  <title><?php print $head_title ?></title>
  <?php print str_replace('rss.xml', 'rss/articles/all', $head); ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if lte IE 7]><?php print phptemplate_get_ie_styles(); ?><![endif]--><!--If Less Than or Equal (lte) to IE 7-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
  <?php if (!empty($admin)) print $admin; ?>
  <div id="outer-wrapper"> 
  <div id="wrapper">    	
  <div id="header">
    <?php print $header; ?>   
    <div class="clear"></div>
  </div> <!-- /#header -->
  <div id="logo_area">	    
    <div id="logo"><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" id="logo" /></a></div>
    <?php if (menu_tree('menu-top-menu')): ?>
      <div id="top-menu"><?php print menu_tree('menu-top-menu'); ?>
        <div class="clear"></div>
        <div id="search_box_top"><?php if ($search_box): ?><?php print $search_box ?><?php endif; ?></div>
      </div>
    <?php endif; ?>		
  </div>        
  <div class="clear"></div>		

  <div id="nav">
    <?php if (isset($primary_links)) : ?>
      <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
    <?php endif; ?>
    <?php if (isset($secondary_links)) : ?>
      <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
    <?php endif; ?>
  </div> <!-- /#nav -->

  <div id="container">
