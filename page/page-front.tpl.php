<?php
/**
 * @file page-front.tpl.php
 * Homepage template.
 * @ingroup page
 */
?>
<?php require_once(dirname(__FILE__) . '/page.header.inc'); ?>

<?php $showhelp = variable_get('openpublish_show_help', TRUE); ?>
<?php if ($showhelp && user_access('administer nodes')): ?>
  <div class="messages status">
    <ul>
      <li><?php print t('Not sure what to do next? See:'); ?> <?php print l(t('Getting Started'), "getting-started", array('alias' => TRUE)); ?> (<?php print l(t('hide this'), "admin/settings/openpublish/clear-help"); ?>)</li>
    </ul>
  </div>
<?php endif; ?>

<div id="homepage-left">
  <?php print $left; ?>
</div>
<div id="homepage-center">
  <?php print $over_content; ?>
</div>
<div id="homepage-right">
	<?php
    $tabs_block = array();
		$tabs_block['articles'] = array(
			'#type' => 'tabset',
		);
		$tabs_block['articles']['tab1'] = array(
			'#type' => 'tabpage',
			'#title' => t('Most Viewed'),
			'#content' => '<h3>Most Viewed</h3>'.views_embed_view('most_viewed_by_node_type', 'block_1', 'article'),
		);
		$tabs_block['articles']['tab2'] = array(
			'#type' => 'tabpage',
			'#title' => t('Most Commented'),
			'#content' => '<h3>Most Commented</h3>'.views_embed_view('most_commented_articles', 'block_1'),
		);
		print tabs_render($tabs_block);
	?>
	<br/>
  <?php print $right; ?>
</div>

<?php require_once(dirname(__FILE__) . '/page.footer.inc');
