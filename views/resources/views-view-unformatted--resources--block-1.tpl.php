<h3><?php print l('Resources', $view->display['page_1']->display_options['path']); ?></h3>

<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes[$id]; ?>">
    <?php print $row; ?>
  </div>
  <?php if (stripos($classes[$id],'views-row-last') === false): ?><div class="views-separator"></div><?php endif; ?>
<?php endforeach; ?>