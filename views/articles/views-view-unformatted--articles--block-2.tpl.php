<h3><?php print l('Latest Headlines', $view->display['page_1']->display_options['path']); ?></h3>

<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes[$id]; ?>">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
<div class="homepage-featured-separator"></div>
