<?php

function openpublish_theme() {
  return array(
    // The form ID.
    'search_theme_form' => array(
      // Forms always take the form argument.
      'arguments' => array('form' => NULL),
    ),
  );
}

function openpublish_search_theme_form($form) {
  $output = '<div id="search" class="container-inline">';
  unset($form['submit']);
  $form['submit'] = array(
    '#type' => 'image_button',
    '#name' => 'op', 
    '#prefix' => '<div id="top-search-image">',
    '#suffix' => '</div>',
    '#button_type' => 'submit', 
    '#submit' => TRUE,
    '#src' => path_to_theme().'/images/button_search.gif',
  );
  $output .= drupal_render($form);
  $output .= '</div>';
  return $output;
}

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
 
function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
// uncomment the next line to enable current page in the breadcrumb trail
//    $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function phptemplate_comment_submitted($comment) {
  return t('by <strong>!username</strong> | !datetime',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function phptemplate_node_submitted($node) {
  return t('by <strong>!username</strong> | !datetime',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generates IE CSS links.
 */
function phptemplate_get_ie_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/css/fix-ie.css" />';
  return $iecss;
}

function phptemplate_get_ie6_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/css/fix-ie6.css" />';
  return $iecss;
}

/**
 * Adds even and odd classes to <li> tags in ul.menu lists
 */ 
function phptemplate_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  static $zebra = FALSE;
  $zebra = !$zebra;
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  if ($zebra) {
    $class .= ' even';
  }
  else {
    $class .= ' odd';
  }
  return '<li class="'. $class .'">'. $link . $menu ."</li>\n";
}

function openpublish_tabset($element) {
  $output = '<div id="tabs-'. $element['#tabset_name'] .'"'. drupal_attributes($element['#attributes']) .'>';  
  $output .= $element['#children'];
  $output .= '<ul>';
  foreach (element_children($element) as $key) {
    if ($element[$key]['#type'] && $element[$key]['#type'] == 'tabpage') {
      $output .= '<li'. drupal_attributes($element[$key]['#attributes']) .'><a href="#tabs-'. $element['#tabset_name'] .'-'. $element[$key]['#index'] .'">'. $element[$key]['#title'] .'</a></li>';
    }
  }
  $output .= '</ul>';
  $output .= '</div>';
  return $output;
}

function related_terms_links($taxonomy) {	
	$out_terms = '';
	if (is_array($taxonomy) && !empty($taxonomy)) {
	  $out_terms .= '<div class="related-terms"><strong>Related Terms:</strong>';
    foreach($taxonomy as $term) {  	    	
  	  $out_terms .= '<div class="related-term">'.l($term['title'],'topics/'.$term['title']).'</div>';
    }
    $out_terms .= '</div>';    
  }
  return $out_terms;
}

function phptemplate_addthis_widget() {

$link_title = urlencode($title);

$out = <<<HTML
     <script type="text/javascript">
  	  addthis_pub  = 'openpublish';
  	  addthis_options = 'favorites, email, digg, delicious, myspace, facebook, google, live, more';</script>
  	  <a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, '', '[URL]', '$link_title')" onmouseout="addthis_close()" onclick="return addthis_sendto()"><img src="http://s7.addthis.com/button0-share.gif" width="83" height="16" border="0" alt="" /></a><script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js">
  	  </script>

HTML;

return $out;

}
